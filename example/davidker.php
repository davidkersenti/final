﻿

<!DOCTYPE html>
<html lang="he">

<head>
	<title>דף הבית</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="stylep.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body class="body">

	<header id="mainheader">
		<img src="photos/logo.JPG">
		<nav>
			<ul>
				<li class="active"><a href="#">דף הבית</a></li>
				<li><a href="#">אודותינו</a></li>
				<li><a href="#">פרופיל עבודות</a></li>
				<li><a href="formpage.php">להרשמה</a></li>		
			</ul>
		</nav>
		
		<span>שלום</span>
					<span>אורח</span>
		 		
	</header>
	
	<section id="maincontent">
		<article class="bigcontent">
			<header>
				<h2>כתבה ראשונה</h2>
			</header>
			<footer>
				<p class="post-info">נכתב על ידינו ב- 26/02/2014</p>
			</footer>
			<content>
				<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק  
				שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, 
				ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורג
				ם בורק? לתיג ישבעס.</p>
				<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק  
				שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, 
				ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורג
				ם בורק? לתיג ישבעס.</p>
			</content>
		</article>
		<article class="bigcontent">
			<header>
				<h2>כתבה שניה</h2>
			</header>
			<footer>
				<p class="post-info">נכתב על ידינו ב- 26/02/2014</p>
			</footer>
			<content>
				<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק  
				שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, 
				ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורג
				ם בורק? לתיג ישבעס.</p>
				<p>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק  
				שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, 
				ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורג
				ם בורק? לתיג ישבעס.</p>
			</content>
		</article>
	</section>
	
	<section id="sidebar">
		<article class="smallcontent">
			<h2>פרטים אישיים</h2>
			<p>שם פרטי:<input type="text" name="first><br>
			    <div class="form-group"> 
                    <label for="firstname">שם פרטי:</label> 
                    <input type="text" name="firstname" class="form-control requiredField" id="firstname"> 
                </div> 
                <div class="form-group"> 
                    <label for="lastname">שם משפחה:</label> 
                    <input type="text" name="lastname" class="form-control requiredField" id="lastname"> 
                     
                </div> 
                <div class="form-group"> 
                    <label for="username">שם משתמש:</label> 
                    <input type="text" name="username" class="form-control requiredField" id="username"> 
                </div> 
                <div class="form-group"> 
                    <label for="email">כתובת דואר אלקטרוני:</label> 
                    <input type="email" name="email" class="form-control requiredField email" id="email"> 
                </div> 
                <div class="form-group"> 
                    <label for="pwd">הקש סיסמה:</label> 
                    <input type="password" name="password" class="form-control" id="pwd"> 
                </div> 
                <div class="form-group"> 
                    <label for="reTypePassword">הקש סיסמה שוב:</label> 
                    <input type="password" name="reTypePassword" class="form-control" id="rpwd"> 
                </div> 
                <label><input type="radio" name="sex" value="male">זכר</label> 
                <label><input type="radio" name="sex" value="female">נקבה</label> 
                 
                <!--<input type="radio" name="sex" value="male">זכר 
                    <input type="radio" name="sex" value="female">נקבה</li>--> 
            <fieldset> 
            <legend>כלי תחבורה זמין</legend> 
            <div> 
                <label><input type="checkbox" name="vehicle" value="Bicycle">אופניים</label> 
            </div> 
            <div> 
                <label><input type="checkbox" name="vehicle" value="Car">רכב פרטי</label> 
            </div> 
             
			
			
		<article class="smallcontent">
			<h2>כתבה צידית אמצעית</h2>
			<p>ורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק</p>
		</article>
				
		<article class="smallcontent">
			<h2>כתבה צידית תחתונה</h2>
			<p>ורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית להאמית קרהשק</p>
		</article>
	</section>
	
	<footer id="mainfooter">
		<p>במסגרת הקורס תכנות בסביבת אינטרנט</p>
	</footer>

</body>

</html>