body { 
    direction: rtl; 
    background-color: #F0F0F0; 
    color: #000305; 
    font-size: 87.5%; /* base font size is 14 px */ 
    font-family: Arial; 
    line-height: 1.5; 
    text-align: right; 
} 

a { 
    text-decoration: none; 
     
} 

.body { 
    margin: 0 auto; 
    width: 70%; 
    clear: both; 
} 

#mainheader img { 
    width: 30%; 
    height: auto; 
    margin: 2% 0; 
} 

#mainheader nav { 
    background-color: #666; 
    height: 41px;    /*�� �� ������ �- 40*/ 
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px; 
} 

#mainheader nav ul { 
    list-style: none; 
    margin: 0 auto; 
} 

#mainheader nav ul li { 
    float: right; 
    display: inline; 
} 

#mainheader nav a:link, #mainheader nav a:visited { 
        color: #FFF; 
        font-weight:bold; 
        display: inline-block; 
        padding: 10px 25px; 
        /*height: 20px;     �� �� ������*/ 
} 

#mainheader nav a:hover, #mainheader nav a:active, 
#mainheader nav .active a:link, #mainheader nav .active a:visited{ 
        background-color: #CF5C3F; 
        text-decoration: none; 
} 

#mainheader nav ul li a { 
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px; 
} 

#maincontent { 
    line-height: 25px; 
    width: 70%; 
    float: right; 
} 
  
.post-info { 
    font-style: italic; 
    color: #999; 
    font-size: 85%; 
 } 
  
 .bigcontent { 
    background-color: #FFF; 
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px; 
    padding: 3% 5%; 
    margin-top: 2%; 
 } 
  
 .smallcontent { 
    width: 21%; 
    float: left; 
    background-color: #FFF; 
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px; 
    padding: 2% 3%; 
    margin-top: 2%; 
    margin-bottom: 2%; 
 } 
  
 #mainfooter { 
    width: 100%; 
    height: 40px; 
    float: right; 
    border-radius: 5px; 
    -moz-border-radius: 5px; 
    -webkit-border-radius: 5px; 
    background-color: #666; 
    margin-top: 2%; 
    margin-bottom: 2%; 
 } 
  
 #mainfooter p { 
    width: 92%; 
    margin: 10px auto; 
    color: #FFF; 
    font-weight:bold; 
 } 

 .error { 
    display:block; 
    color:red; 
 } 
  
@media only screen and (min-width: 150px) and (max-width: 600px) 
{ 
    .body { 
        width: 90%; 
        font-size: 95%; 
    } 
     
    #mainheader img { 
        width: 100%; 
    } 

    #mainheader nav { 
        height: 160px; 
    } 

    #mainheader nav ul { 
        padding-right: 0; 
    } 

    #mainheader nav ul li { 
        width: 100%; 
        text-align: center; 
    } 

    #mainheader nav a:link, #mainheader nav a:visited { 
        display: block; 
    } 
    #maincontent { 
        width: 100%; 
        margin-top: 2%; 
    } 
     
    .post-info { 
        display: none; 
    } 

    .bigcontent { 
        margin-top: 2%; 
        margin-bottom: 2%; 
    } 
      
    .smallcontent { 
        width: 94%; 
        padding: 2% 3%; 
        margin-top: 2%; 
        margin-bottom: 2%; 
     } 
      
} 

/*bootstrapoverride*/ 
/*����� ��� ������ �� �������� ������� ������� ����� �����*/ 
html{ 
    font-size: 100% 
} 
#mainform{ 
    line-height: 25px; 
    width: 80%; 
    float: right; 
} 