$(document).ready(function(){
	$("#menu a").click(function(){
		if ($(this).text()=="html" ){
			if ($("form input[value='html']").is(':checked')) {
				$("form input[value='html']").attr('checked', false);
			}
			else {
				$("form input[value='html']").attr('checked', true);
			}
		}
	});
	
	$("form input[value='css']").change(function() {
		if(this.checked) {
			$( "#menu a:contains('CSS')" ).css( "text-decoration", "underline" );
		}
		else {
			$( "#menu a:contains('CSS')" ).css( "text-decoration", "none" );
		}
	});
	
		if ($("#formID input:checkbox:checked").length > 0)
		{
			// any one is checked
		}
		else
		{
		   // none is checked
		}
		
	$("form").submit(function(){
		$("form .error").remove();
		
		var hasError = false;
		if(!$('input[type="checkbox"]').is(':checked'))
		
		{
			$("input[type='checkbox']").parent().append('<span class="error">you need to check at least one</span>');
			hasError = true;
		}
		
		if(!hasError) {
		
			$(this).submit();
		}
		
		
		return false;
	
	});

	
});