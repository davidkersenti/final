<?php

use yii\db\Migration;

class m170831_082759_init_table_project extends Migration
{
public function up()
   {

     $this->createTable('project', [
             'projectId'  => 'pk',
			'projectName' => 'string',
            'teamleader' => 'integer',
			'startDate' => 'date',
            'planeDate'  => 'date' ,
			'endDate' => 'date',
            'urgency'  => 'integer', // פתח זר
            'status' => 'integer', ///זר
            'location' => 'string',
            'description'  => 'text', 
            'created_at'=> $this->integer(),
				'updated_at'=> $this->integer(),
				'created_by'=> $this->integer(),
				'updated_by'=> $this->integer(),

		]);

         $this->addForeignKey(
            'fk-project-teamleader',// This is the fk => the table where i want the fk will be
            'project',// son table
            'teamleader', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
			);

           

            $this->addForeignKey(
            'fk-project-urgency',// This is the fk => the table where i want the fk will be
            'project',// son table
            'urgency', // son pk	
            'urgency', // father table
            'urgencyId', // father pk
            'CASCADE'
			);

            $this->addForeignKey(
            'fk-project-status',// This is the fk => the table where i want the fk will be
            'project',// son table
            'status', // son pk	
            'status', // father table
            'statusId', // father pk
            'CASCADE'
			);
            
    }
    public function down()
    {
       $this->dropTable('project');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_082759_init_table_project cannot be reverted.\n";

        return false;
    }
    */
}
