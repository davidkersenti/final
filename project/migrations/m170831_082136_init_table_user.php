<?php

use yii\db\Migration;

class m170831_082136_init_table_user extends Migration
{
    public function up()
    {
	             
                  $this->createTable('user', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'username' => $this->string(),
            'password' => $this->string(),
            'role'  => $this->integer(), ////מפתח זר
            'auth_Key' => $this->string(),
            'phoneNumber' => $this->integer(),
            'age' => $this->integer(),
            'email' => $this->string(),
            'address' => $this->string(),
				'created_at'=> $this->integer(),
				'updated_at'=> $this->integer(),
				'created_by'=> $this->integer(),
				'updated_by'=> $this->integer(),
				],
				 'ENGINE=InnoDB'
			);
				 $this->addForeignKey(
            'fk-user-role',// This is the fk => the table where i want the fk will be
            'user',// son table
            'role', // son pk	
            'role', // father table
            'roleId', // father pk
            'CASCADE'
			);
             
        
    }
    public function safeDown()
    {
         $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_082136_init_table_user cannot be reverted.\n";

        return false;
    }
    */
}
