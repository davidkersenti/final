<?php

use yii\db\Migration;

class m170831_082533_init_table_urgency extends Migration
{
      public function up()

    {

        $this->createTable('urgency', [
            'urgencyId' => 'pk' ,
            'urgencyName'  => 'string',
            
		]);
   
    }

    
    public function down()
    {
     $this->dropTable('urgency');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_082533_init_table_urgency cannot be reverted.\n";

        return false;
    }
    */
}
