<?php

use yii\db\Migration;

class m170831_081611_init_table_role extends Migration
{
       public function up()

    {
        $this->createTable('role', [
            'roleId' => 'pk' ,
            'roleName'  => 'string',
            
		]);
   }

     public function down()
    {
        $this->dropTable('role');

       
    }  


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_081611_init_table_role cannot be reverted.\n";

        return false;
    }
    */
}
