<?php

use yii\db\Migration;

class m170831_083812_init_table_taskproject extends Migration
{
     public function up()
    {

        $this->createTable(
            'taskproject',
            [
                'taskid' => 'integer',
                'projectid' => 'integer',
                'PRIMARY KEY(taskid, projectid)',
             
            ],
            'ENGINE=InnoDB'
        );

         $this->addForeignKey(
            'fk-taskproject-taskid',// This is the fk => the table where i want the fk will be
            'taskproject',// son table
            'taskid', // son pk	
            'task', // father table
            'TaskId', // father pk
            'CASCADE'
        );	

       $this->addForeignKey(
            'fk-taskproject-projectid',// This is the fk => the table where i want the fk will be
            'taskproject',// son table
            'projectid', // son pk	
            'project', // father table
            'projectId', // father pk
            'CASCADE'
        );


    }

    public function down()
    {
         $this->dropTable('taskproject');

        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_083812_init_table_taskproject cannot be reverted.\n";

        return false;
    }
    */
}
