<?php

use yii\db\Migration;

class m170831_082416_init_table_level extends Migration
{
     public function up()

    {
        $this->createTable('level', [
            'levelId' => 'pk' ,
            'levelname'  => 'string',
            
		]);
   }

    public function safeDown()
    {
         $this->dropTable('level');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_082416_init_table_level cannot be reverted.\n";

        return false;
    }
    */
}
