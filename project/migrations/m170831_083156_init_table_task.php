<?php

use yii\db\Migration;

class m170831_083156_init_table_task extends Migration
{
   public function up()
    {

             $this->createTable('task', [
            'taskId'  => 'pk',
			'taskName' => 'string',
            'project' => 'integer', ///מפתח זר
			'startDate' => 'date',
            'planeDate'  => 'date' ,
			'endDate' => 'date',
            'level'  => 'integer', // פתח זר
            'status' => 'integer', ///מפתח זר
            'description'  => 'text',
            'created_at'=> $this->integer(),
				'updated_at'=> $this->integer(),
				'created_by'=> $this->integer(),
				'updated_by'=> $this->integer(),

		]);


             $this->addForeignKey(
            'fk-task-project',// This is the fk => the table where i want the fk will be
            'task',// son table
            'project', // son pk	
            'project', // father table
            'projectId', // father pk
            'CASCADE'


			); $this->addForeignKey(
            'fk-task-level',// This is the fk => the table where i want the fk will be
            'task',// son table
            'level', // son pk	
            'level', // father table
            'levelId', // father pk
            'CASCADE'
			);
        
        	

             
               $this->addForeignKey(
            'fk-task-status',// This is the fk => the table where i want the fk will be
            'task',// son table
            'status', // son pk	
            'status', // father table
            'statusId', // father pk
            'CASCADE'
			);


    }

    public function down()
    {
       $this->dropTable('task');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_083156_init_table_task cannot be reverted.\n";

        return false;
    }
    */
}
