<?php

use yii\db\Migration;

class m170831_081927_init_table_status extends Migration
{
      public function up()
    {

        $this->createTable('status', [
            'statusId' => 'pk' ,
            'statusName'  => 'string',
            
		]);
  }

    public function down()
    {
         $this->dropTable('status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_081927_init_table_status cannot be reverted.\n";

        return false;
    }
    */
}
