<?php

use yii\db\Migration;

class m170831_083643_init_table_usertask extends Migration
{
   public function up()
    {

        $this->createTable(
            'usertask',
            [
                'userid' => 'integer',
                'taskid' => 'integer',
                'PRIMARY KEY(userid, taskid)',
             
            ],
            'ENGINE=InnoDB'
        );

         $this->addForeignKey(
            'fk-usertask-userid',// This is the fk => the table where i want the fk will be
            'usertask',// son table
            'userid', // son pk	
            'user', // father table
            'id', // father pk
            'CASCADE'
        );	

       $this->addForeignKey(
            'fk-usertask-taskid',// This is the fk => the table where i want the fk will be
            'usertask',// son table
            'taskid', // son pk	
            'task', // father table
            'taskId', // father pk
            'CASCADE'
        );


    }

    public function down()
    {
         $this->dropTable('usertask');

        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_083643_init_table_usertask cannot be reverted.\n";

        return false;
    }
    */
}
