<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;

use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $levelId
 * @property string $levelname
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['levelname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'levelId' => 'Level ID',
            'levelname' => 'Levelname',
        ];
    }
    	public static function getLevels()
	{
		$allLevels = self::find()->all();
		$allLevelsArray = ArrayHelper::
					map($allLevels, 'levelId', 'levelname');
		return $allLevelsArray;						
	}
    

}
