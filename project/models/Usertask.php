<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usertask".
 *
 * @property integer $userid
 * @property integer $taskid
 *
 * @property Task $task
 * @property User $user
 */
class Usertask extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usertask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'taskid'], 'required'],
            [['userid', 'taskid'], 'integer'],
            [['taskid'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskid' => 'taskId']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'taskid' => 'Taskid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['taskId' => 'taskid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
