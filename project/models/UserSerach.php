<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSerach represents the model behind the search form about `app\models\User`.
 */
class UserSerach extends User
{
    public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role', 'phoneNumber', 'age', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['firstname', 'lastname','globalSearch', 'username', 'password', 'auth_Key', 'email', 'address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
         
        // grid filtering conditions
       

        $query->andFilterWhere(['like', 'firstname', $this->globalSearch])
            ->orFilterWhere(['like', 'lastname', $this->globalSearch])
            ->orFilterWhere(['like', 'username', $this->globalSearch])
            ->orFilterWhere(['like', 'password', $this->globalSearch])
            ->orFilterWhere(['like', 'auth_Key', $this->globalSearch])
            ->orFilterWhere(['like', 'email', $this->globalSearch])
            ->orFilterWhere(['like', 'address', $this->globalSearch]);

        return $dataProvider;
    }
}
