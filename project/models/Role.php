<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use app\models\User;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $roleId
 * @property string $roleName
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roleName'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'roleId' => 'Role ID',
            'roleName' => 'Role Name',
        ];
    }
    public static function getRolesss()
	{
		$allRoles = self::find()->all();
		$allRolesArray = ArrayHelper::
					map($allRoles, 'roleId', 'roleName');
		return $allRolesArray;						
	}
       
}
