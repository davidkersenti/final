<?php

namespace app\models;

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;
use app\models\Status;


/**
 * TaskSerach represents the model behind the search form about `app\models\Task`.
 */
class TaskSerach extends Task
{

    public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskId', 'project', 'level', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['taskName', 'startDate','globalSearch', 'planeDate', 'endDate', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       $this->status == -1 ? $this->status = null : $this->status; 
       
        

        // grid filtering conditions
       

        $query->orFilterWhere(['like', 'taskName', $this->globalSearch])
            ->orFilterWhere(['like', 'description', $this->globalSearch]);

        return $dataProvider;
    }
}
