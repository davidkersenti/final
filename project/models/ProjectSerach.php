<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSerach represents the model behind the search form about `app\models\Project`.
 */
class ProjectSerach extends Project
{


    public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectId', 'teamleader', 'urgency', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['projectName','globalSearch', 'startDate', 'planeDate', 'endDate', 'location', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
    //    $this->status == -1 ? $this->status = null : $this->status; 

        // grid filtering conditions
    

        $query->orFilterWhere(['like', 'projectName', $this->globalSearch])
            ->orFilterWhere(['like', 'location', $this->globalSearch])
            ->orFilterWhere(['like', 'description', $this->globalSearch]);

        return $dataProvider;
    }
}
