<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UrlManager\Url;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
         //////////////////////////////////// RBAC ////////////////////////////////////////
    
public function actionRole()
	{
		$auth = Yii::$app->authManager;	

        $ceo = $auth->createRole('ceo');
		$auth->add($ceo);			
		
		$task_perform = $auth->createRole('task_perform');
		$auth->add($task_perform);
		
		$project_manager = $auth->createRole('project_manager');
		$auth->add($project_manager);

    	$admin = $auth->createRole('admin');
		$auth->add($admin);
	}


    public function actionCeopermissions()
	{
		$auth = Yii::$app->authManager;

		$viewProject = $auth->createPermission('viewProject'); 
		$viewProject->description = 'View Project ';
		$auth->add($viewProject);

        $indexProject = $auth->createPermission('indexProject'); 
		$indexProject->description = 'Index Project ';
		$auth->add($indexProject);   

        $viewTask = $auth->createPermission('viewTask'); 
		$viewTask->description = 'viewTask';
		$auth->add($viewTask);

        $indexTask = $auth->createPermission('indexTask'); 
		$indexTask->description = 'Index Task ';
		$auth->add($indexTask);

        $viewUser = $auth->createPermission('viewUser'); 
		$viewUser->description = 'View User';
		$auth->add($viewUser);

        $indexUser = $auth->createPermission('indexUser'); 
		$indexUser->description = 'Index User ';
		$auth->add($indexUser);

	}

	public function actionTaskpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$changeStatus = $auth->createPermission('changeStatus');
		$changeStatus->description = 'Change the status task';
		$auth->add($changeStatus);

        

         $editTask = $auth->createPermission('editTask'); 
		$editTask->description = 'Edit Task ';
		$auth->add($editTask);		
		
	}


	public function actionManagerpermissions()
	{
		$auth = Yii::$app->authManager;

		$assignUserToProject = $auth->createPermission('assignUserToProject'); 
		$assignUserToProject->description = 'Assign User To Project';
		$auth->add($assignUserToProject);	

		$assignUserToTask = $auth->createPermission('assignUserToTask'); 
		$assignUserToTask->description = 'Assign User To Task';
		$auth->add($assignUserToTask);	

        $createTask = $auth->createPermission('createTask'); 
		$createTask->description = 'Create Task ';
		$auth->add($createTask);

        $deleteTask = $auth->createPermission('deleteTask'); 
		$deleteTask->description = 'Delete Task ';
		$auth->add($deleteTask);

        $createProject = $auth->createPermission('createProject'); 
		$createProject->description = 'Create Project ';
		$auth->add($createProject);
	}

	
	public function actionAdminpermissions()
	{
		$auth = Yii::$app->authManager;
		
		

		$deleteProject = $auth->createPermission('deleteProject');  
		$deleteProject->description = 'Admin can delete projects';
		$auth->add($deleteProject);

        $editProject = $auth->createPermission('editProject');  
		$editProject->description = 'Admin can edit Project';
		$auth->add($editProject);

        $createUser = $auth->createPermission('createUser'); 
		$createUser->description = 'Admin can create users';
		$auth->add($createUser);

        

         $editUser = $auth->createPermission('editUser'); 
		$editUser->description = 'Admin can edit users';
		$auth->add($editUser);

        $deleteUser = $auth->createPermission('deleteUser'); 
		$deleteUser->description = 'Admin can delete users';
		$auth->add($deleteUser);
        
	
	}

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				

        $ceo = $auth->getRole('ceo');

        $viewProject = $auth->getPermission('viewProject'); 
		$auth->addChild($ceo, $viewProject); 
         

        $indexProject = $auth->getPermission('indexProject'); 
        $auth->addChild($ceo, $indexProject); 

        $indexTask = $auth->getPermission('indexTask'); 
        $auth->addChild($ceo, $indexTask); 

        $viewTask = $auth->getPermission('viewTask'); 
        $auth->addChild($ceo, $viewTask); 

        $indexUser = $auth->getPermission('indexUser'); 
        $auth->addChild($ceo, $indexUser); 

          $viewUser = $auth->getPermission('viewUser'); 
        $auth->addChild($ceo, $viewUser); 


        ////////////////
        $task_perform = $auth->getRole('task_perform'); 

        $auth->addChild($task_perform, $ceo);

		$changeStatus = $auth->getPermission('changeStatus'); 
		$auth->addChild($task_perform, $changeStatus);
		
		$editTask = $auth->getPermission('editTask'); 
		$auth->addChild($task_perform, $editTask);

       


        /////////////////////////////


		$project_manager = $auth->getRole('project_manager'); 
		
        $auth->addChild($project_manager, $task_perform);
		
		$assignUserToProject = $auth->getPermission('assignUserToProject'); 
		$auth->addChild($project_manager, $assignUserToProject);

		$assignUserToTask = $auth->getPermission('assignUserToTask'); 
		$auth->addChild($project_manager, $assignUserToTask);	

        $createTask = $auth->getPermission('createTask'); 
		$auth->addChild($project_manager, $createTask);

        $editTask = $auth->getPermission('editTask'); 
		$auth->addChild($project_manager, $editTask);

        $deleteTask = $auth->getPermission('deleteTask'); 
		$auth->addChild($project_manager, $deleteTask);

        $createProject = $auth->getPermission('createProject'); 
		$auth->addChild($project_manager, $createProject);
        

        

        
		///////////////////////////////////////
		
		$admin = $auth->getRole('admin'); 
		$auth->addChild($admin, $project_manager);	
		
		
			
	   $deleteProject = $auth->getPermission('deleteProject'); 
		$auth->addChild($admin, $deleteProject);
		
		$editProject = $auth->getPermission('editProject'); 
		$auth->addChild($admin, $editProject);

        $createUser = $auth->getPermission('createUser'); 
		$auth->addChild($admin, $createUser);

        $deleteUser = $auth->getPermission('deleteUser'); 
		$auth->addChild($admin, $deleteUser);

        $editUser = $auth->getPermission('editUser'); 
		$auth->addChild($admin, $editUser);

        $viewUser = $auth->getPermission('viewUser'); 
		$auth->addChild($admin, $viewUser);

        $indexUser = $auth->getPermission('indexUser'); 
		$auth->addChild($admin, $indexUser);

	}

}
