<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Role;
use app\models\Specialization;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

       <?= $form->field($model, 'password')->passwordInput(['placeholder' => "
At least 3 letters "]) ?>

    
    <?= $form->field($model, 'role')->
 
				dropDownList(Role::getRolesss())  ?>

             

    <!--<?= $form->field($model, 'auth_Key')->textInput(['maxlength' => true]) ?>-->

    <?= $form->field($model, 'phoneNumber')->textInput() ?>

    <?= $form->field($model, 'age')->textInput(['placeholder' => "Age between 18-60 "]) ?>

     <?= $form->field($model, 'email')->textInput(['placeholder' => "This field is recommended for password recovery"],['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <!--<?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
