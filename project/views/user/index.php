<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/user.jpg" class="img-rounded" height="120" width="360" style="float: right;">
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
      
       <?= Html::button('Create User',  ['value' =>Url::to('index.php?r=user/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <?php
    Modal::begin([
'header'=>'<h4>User</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            'username',
            // 'password',
            // 'role',
            // 'auth_Key',
            // 'phoneNumber',
            // 'age',
            // 'email:email',
            // 'address',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
               

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
