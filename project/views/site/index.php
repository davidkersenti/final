<?php
use yii\bootsrap\Model;

/* @var $this yii\web\View */

$this->title = 'Project Management';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Welcome To Project Management System!</h2>
        <p class="lead">The Effective Way To Manage Your Projects</p> 
        <img id="contactform-verifycode-image" src="/project/image/home.jpg" alt="school" height="400" width="1000">
        <p><a class="btn btn-link" style="width:200px;" href="?r=site/login">Get Started</a> 
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">

 
                <h2>Project</h2>
                <p>For your buisness is nesseary to create projects. In this page you can manage your project: create,update and delete. For create project you need to assign users, Team Leader, Setting schedules,Set status and set the urgency.Since this will give you the best view for managing your projects.
            </p>


                <p><a class="btn btn-primary" href="?r=project/index">View Projects</a></p>

            </div>
            <div class="col-lg-4">
                <h2>Tasks</h2>

                <p>Tasks must be selected for project execution. In this page you can manage your tasks: create,update and delete. For create tasks you need to assign only users that belongs to the project, Setting schedules,Set status and more.</p>

                <p><a class="btn btn-lg btn-success" href="?r=task/index">View Tasks</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Users</h2>

                <p>Each project has to select users and define roles: manager, task performer, user and more. In Addition you will give each user his own authorization in order to keep maintaining classified information</p>

                <p><a class="btn btn-info" href="?r=user/index">View Users</a></p>
            </div>
        </div>

    </div>
</div>

