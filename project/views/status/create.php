
<!DOCTYPE html>
<html>
<body>
<img src="/project/image/perform.jpg" class="img-rounded" height="300" width="300" style="float: right;">

<article>
  <h1>Task Performing</h1>
  <p>Task performing is the process of managing a task through its life cycle. It involves planning, testing, tracking and reporting. Task performing can help either individuals achieve goals, or groups of individuals collaborate and share knowledge for the accomplishment of collective goals. Tasks are also differentiated by complexity, from low to high.
Effective task performing requires managing all aspects of a task, including its status, priority, time, human and financial resources assignments, recurrency, notifications and so on. These can be lumped together broadly into the basic activities of task management.
Managing multiple individual or team tasks may require specialised software, for example workflow or project management software. In fact, many people believe that task performing should serve as a foundation for project management activities.
Task performing may form part of project management and process management and can serve as the foundation for efficient workflow in an organisation. Project managers adhering to task-oriented management have a detailed and up-to-date project schedule, and are usually good at directing team members and moving the project forward.</p>
  <p><strong>About the role in the system:</strong> In this systsem the permissions of task performing is to view and change the status in the task that he have a responsibility for them. </p>
 


  
</article>




</body>
</html>