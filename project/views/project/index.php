<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/project.jpg" class="img-rounded" height="100" width="324" style="float: right;">
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <?= Html::button('Create Project',  ['value' =>Url::to('index.php?r=project/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <!--pop up-->
    <?php
    Modal::begin([
'header'=>'<h4>Project</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'projectId',
            'projectName',
            'teamleader',
            'startDate',
            'planeDate',
            // 'endDate',
            // 'urgency',
            // [
			// 	'attribute' => 'status',
			// 	'label' => 'Status',
			// 	'value' => function($model){
			// 		return $model->status0->statusName;
			// 	},
			// 	'filter'=>Html::dropDownList('ProjectSearch[status]', $status, $statuses, ['class'=>'form-control']),
			// ],
            'location',
             'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
             [
                'attribute' => 'users',
                'label' => 'Users',
                'format' => 'raw',

                'value' => function($model){
                    return $model->Usersnames2; //This function in Activity.php

                },
            ],
             

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
