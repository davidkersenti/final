<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="/project/image/task.jpg" class="img-rounded" height="120" width="324" style="float: right;">
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Create Task',  ['value' =>Url::to('index.php?r=task/create'), 'class' => 'btn btn-success','id'=>'modalButton']) ?>
    </p>
    <!--pop up-->
    <?php
    Modal::begin([
'header'=>'<h4>Task</h4>',
'id' => 'modal',
'size' => 'modal-lg'
    ]);

    echo "<div id ='modalContent'></div>";
    Modal::end();

    ?>

    
        
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'taskId',
            'taskName',
            
            'startDate',
            'planeDate',
            'endDate',
            // 'level',
             [
				'attribute' => 'status',
				'label' => 'Status',

				'format' => 'raw',
				'value' => function($model){
					return $model->status0->statusName;
				},
				'filter'=>Html::dropDownList('TaskSearch[status]', $status, $statuses, ['class'=>'form-control']),
			],
                
            'description:ntext',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
              [
                'attribute' => 'users',
                'label' => 'Users',
                'format' => 'raw',

                'value' => function($model){
                    return $model->Usersnames2; //This function in Activity.php

                },
            ],
             


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
